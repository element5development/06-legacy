<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title">
	<section>
		<h1>Success</h1>
	</section>
</header>

<main>
	<article class="paper-container">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/paw-print.svg" alt="paw print icon" />
		<section>
			<?php if ( get_field('page_title') ) : ?>
				<h2><?php the_field('page_title'); ?></h2>
			<?php else : ?>
				<h2><?php the_title(); ?></h2>
			<?php endif; ?>
			<p><?php the_field('title_description'); ?></p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>