<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	custom page template and design to display a single form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title">
	<section>
		<h1><?php the_title(); ?></h1>
	</section>
</header>

<main>
	<article class="paper-container">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/paw-print.svg" alt="paw print icon" />
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>