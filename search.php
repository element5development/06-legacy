<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header-search'); ?>

<main>

	<article> 
		<section class="intro has-normal-width">
			<p><b>Search Results (<?php echo $wp_query->found_posts; ?>)</b></p>
		</section>
		<?php if ( have_posts() ) : ?>
			<section class="feed feed-search has-normal-width">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('template-parts/elements/previews/preview-search'); ?>
				<?php endwhile; ?>
			</section>
			<section class="infinite-scroll has-normal-width">
				<div class="page-load-status">
					<p class="infinite-scroll-request">
						<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
							<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
								<animateTransform attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 25 25"
									to="360 25 25"
									dur="0.6s"
									repeatCount="indefinite"/>
							</path>
						</svg>
					</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error"></p>
				</div>
				<?php the_posts_pagination( array(
					'prev_text'	=> __( 'Previous page' ),
					'next_text'	=> __( 'Next page' ),
				) ); ?>
				<a class="load-more button">View more</a>
			</section>
		<?php else : ?>
			<section class="has-normal-width">
				<p>We were unable to find any results for "<?php echo get_search_query(); ?>"</p>
			</section>
		<?php endif; ?>
		<hr>
		<section class="call-to-actions has-small-width">
			<div>
				<h3>Helpful Links</h3>
				<ul>
					<li><a href="<?php echo get_permalink(389); ?>">Who is 06?</a></li>
					<li><a href="<?php echo get_permalink(455); ?>/#yellowstone-packs">Yellowstone Wolf Packs</a></li>
					<li><a href="<?php echo get_post_type_archive_link('legislation'); ?>">Current Legislation</a></li>
					<li><a href="<?php echo get_permalink(382); ?>">How can I help?</a></li>
				</ul>
			</div>
			<div>
				<h3>Can't Find Something</h3>
				<p>Couldn’t find what you were looking for? Send us a message and we’ll do our best to help.</p>
				<a href="<?php echo esc_url(get_permalink(383)); ?>" class="button">Contact Us</a>
			</div>
		</section>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>