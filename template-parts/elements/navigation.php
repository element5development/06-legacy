<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<?php if ( wp_is_mobile() ) : ?>
	<nav class="primary-nav-mobile">
		<button id="activate-search">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Legacy-Logo.png" alt="Legacy 06 Logo" />
		</a>
		<button id="activate-menu">
			<svg>
				<use xlink:href="#menu" />
			</svg>
		</button>
		<div class="menu-items-container">
			<button id="close-menu">
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>	
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
		</div>
		<div class="search-form">
			<button id="close-search">
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
			<?php echo get_search_form(); ?>
		</div>
	</nav>
<?php else : ?>
	<nav class="primary-nav">
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>	
		</div>
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Legacy-Logo.png" alt="Legacy 06 Logo" />
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
			<button id="activate-search">
				<svg id="open">
					<use xlink:href="#search" />
				</svg>
				<svg id="close">
					<use xlink:href="#closed" />
				</svg>
			</button>
		</div>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
	</nav>
<?php endif; ?>