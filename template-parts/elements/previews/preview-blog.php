<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<?php
	if ( has_post_thumbnail() ) :
		$post_image = get_the_post_thumbnail_url(get_the_ID(),'small');
		$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
	else :
		$post_image = get_stylesheet_directory_uri() . '/dist/images/default-post.jpg';
		$alt = 'decorative post preview image';
	endif;
	$cats = get_the_category(get_the_ID());
?>
<article class="preview preview-blog">
	<a href="<?php the_permalink(); ?>"></a>
	<figure>
		<img src="<?php echo $post_image; ?>" alt="<?php echo $alt; ?>" />
	</figure>
	<div>
		<p><?php echo $cats[0]->name; ?></p>
		<h2><?php the_title(); ?></h2>
		<p><?php echo get_excerpt(200); ?></p>
		<div class="button">Continue Reading</div>
	</div>
</article>
