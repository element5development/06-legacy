<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR LEGISLATIONS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-legislation">
	<div class="content">
		<p class="date">OPEN FOR ACTION</p>
		<h2 class="title"><?php the_title(); ?></h2>
		<?php the_content(); ?>
	</div>
</article>
<hr>