<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR PAST LEGISLATIONS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-legislation">
	<div class="content">
		<h2 class="title"><?php the_title(); ?></h2>
		<?php the_content(); ?>
		<?php if( get_field('legislation_status') == 'Passed' ): ?>
			<p class="status"><svg viewBox="0 0 24 24" fill="none" stroke="#417505" stroke-width="4" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="20 6 9 17 4 12"></polyline></svg> <?php the_field('legislation_status'); ?>: <?php the_field('legislation_votes'); ?></p>
		<?php elseif( get_field('legislation_status') == 'Vetoed' ): ?>
			<p class="status"><svg viewBox="0 0 24 24" fill="none" stroke="#d0021b" stroke-width="3" stroke-linecap="butt" stroke-linejoin="arcs"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> <?php the_field('legislation_status'); ?>: <?php the_field('legislation_votes'); ?></p>
		<?php endif; ?>
	</div>
</article>
<hr>