<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<?php
	if ( has_post_thumbnail() ) :
		$background = get_the_post_thumbnail_url(get_the_ID(),'small');
	else :
		$background = get_stylesheet_directory_uri() . '/dist/images/default-post.jpg';
	endif;
	$cats = get_the_category(get_the_ID());
?>
<article class="preview preview-galleries" style="background-image: url('<?php echo $background; ?>');">
	<a href="<?php the_permalink(); ?>"></a>
	<div>
		<h2><?php the_title(); ?></h2>
		<?php if ( $post->post_excerpt ) : ?>
			<p><?php echo get_the_excerpt(); ?></p>
		<?php endif; ?>
	</div>
</article>
