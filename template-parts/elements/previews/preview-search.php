<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR SEARH RESULT

\*----------------------------------------------------------------*/
?>

<?php $cats = get_the_category(get_the_ID()); ?>
<article class="preview preview-search">
	<a href="<?php the_permalink(); ?>"></a>
	<div>
		<h2><?php the_title(); ?></h2>
		<?php if ( $cats ) : ?>
			<p class="category"><?php echo $cats[0]->name; ?> <?php echo get_post_type() ?></p>
		<?php endif; ?>
		<?php the_excerpt(); ?>
	</div>
</article>
