<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR LEGISLATIONS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-legislation">
	<div class="content">
		<?php echo get_the_term_list( $post->ID, 'state', '', '', '' ) ?>
		<p class="date">OPEN FOR ACTION</p>
		<h2 class="title"><?php the_title(); ?></h2>
		<?php the_content(); ?>
	</div>
</article>
<hr>