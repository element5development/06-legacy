<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php 
	$post_url = get_permalink();
	$post_content = get_the_excerpt();
	$featured_img = get_home_url() . get_the_post_thumbnail_url();
	$post_title = get_the_title();
?>

<section class="social-share">
	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_url; ?>" title="Share on Facebook">
		<svg>
			<use xlink:href="#facebook" />
		</svg>
	</a>
	<a target="_blank" href="https://twitter.com/home?status=<?php echo $post_content; echo $post_url; ?>" title="Share on Twitter">
		<svg>
			<use xlink:href="#twitter" />
		</svg>
	</a>
	<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" title="Share on Linkedin">
		<svg>
			<use xlink:href="#linkedin" />
		</svg>
	</a>
	<a target="_blank" href="mailto:?&subject=Check out this post <?php echo $post_title; ?>&body=<?php echo $post_url; echo $post_url; ?>" title="Share on Pinterest">
		<svg>
			<use xlink:href="#email" />
		</svg>
	</a>
</section>