<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>

<?php
	$posttype = get_query_var('post_type');
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
	$sticky = get_option( 'sticky_posts' );
	$categories = get_the_category($post->ID);
	$categories[0]->id;
	$args = array(
		'posts_per_page' => 2,
		'post__not_in' => array( get_the_ID() ),
		'ignore_sticky_posts' => 1,
		'cat' => $categories[0]->cat_ID
	);
	$sticky_query = new WP_Query($args);
?>
<?php if ( $sticky_query->have_posts() ) : ?>
	<section class="related-posts">
		<h3>Related Posts</h3>
		<div>
			<?php while ( $sticky_query->have_posts() ) : $sticky_query->the_post(); ?>
				<?php get_template_part('template-parts/elements/previews/preview-' . $posttype); ?>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; wp_reset_postdata();	?>