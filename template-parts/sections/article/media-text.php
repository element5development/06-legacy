<?php 
/*----------------------------------------------------------------*\

	MEDIA + TEXT

\*----------------------------------------------------------------*/
?>


<section id="section-<?php echo $template_args['sectionId']; ?>" class="media-text has-large-width media-<?php the_sub_field('position'); ?>">
	<?php $background = get_sub_field('background_image'); ?>
	<figure style="background-image: url('<?php echo $background['url']; ?>');">
		<?php $image = get_sub_field('image'); ?>
		<img class="rellax" data-rellax-speed="-1" data-rellax-percentage="0.5" src="<?php echo$image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
	</figure>
	<div>
		<?php the_sub_field('wysiwyg'); ?>
	</div>
</section>