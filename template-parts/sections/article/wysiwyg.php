<?php 
/*----------------------------------------------------------------*\

	STANDARD WYSIWYG

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="wysiwyg-block has-normal-width">
	<div>
		<?php the_sub_field('wysiwyg'); ?>
	</div>
</section>