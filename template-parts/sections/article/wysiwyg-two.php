<?php 
/*----------------------------------------------------------------*\

	WYSIWYG THAT HAS TWO COLUMNS

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="wysiwyg-block has-normal-width">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<div>
		<?php the_sub_field('left_wysiwyg'); ?>
	</div>
	<div>
		<?php the_sub_field('right_wysiwyg'); ?>
	</div>
</section>