<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>

<?php $bg = get_sub_field('background'); ?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="banner" style="background-image: url('<?php echo $bg['sizes']['xlarge']; ?>');">
	<div>
		<?php if ( get_sub_field('title') ) : ?>
			<h2><?php the_sub_field('title'); ?></h2>
		<?php endif; ?>
		<?php if ( get_sub_field('description') ) : ?>
			<p><?php the_sub_field('description'); ?></p>
		<?php endif; ?>
		<?php $link = get_sub_field('button'); ?>
		<?php if( $link ): ?>
			<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
	</div>
</section>