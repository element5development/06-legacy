<?php 
/*----------------------------------------------------------------*\

	IMAGE GALLERY WITH LIGHTBOX ZOOM

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="gallery has-large-width">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<?php if ( get_sub_field('description') ) : ?>
		<p><?php the_sub_field('description'); ?></p>
	<?php endif; ?>
	<div class="gallery-items">
		<?php $images = get_sub_field('gallery'); ?>
		<?php foreach( $images as $image ): ?>
			<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endforeach; ?>
	</div>
</section>