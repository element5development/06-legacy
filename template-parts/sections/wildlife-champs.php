<?php 
/*----------------------------------------------------------------*\

	PULL IN LATEST 2 BLOG POSTS IN THE WILDLIFE CHAMPION CATEGORY

\*----------------------------------------------------------------*/
?>

<?php
	$args = array(
		'posts_per_page' => 2,
		'cat' => 20,

	);
	$champion_query = new WP_Query($args);
?>
<?php if ( $champion_query->have_posts() ) : ?>
	<hr>
	<section class="champion-posts">
		<h3>Pro-Wildlife Champions</h3>
		<div>
			<?php while ( $champion_query->have_posts() ) : $champion_query->the_post(); ?>
				<?php get_template_part('template-parts/elements/previews/preview-blog'); ?>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; wp_reset_postdata();	?>