<?php 
/*----------------------------------------------------------------*\

	PACK FEED

\*----------------------------------------------------------------*/
?>
	
<?php // Custom WP query crew_members
	$args_crew_members = array(
		'post_type' => array('pack'),
		'posts_per_page' => -1,
	);
	$crew_members = new WP_Query( $args_crew_members );
?>

<?php if ( $crew_members->have_posts() ) : ?>
	<section id="yellowstone-packs" class="packs-feed">
		<h2><?php the_field('packs_title','option'); ?></h2>
		<?php the_field('packs_description','option'); ?>
		<div class="key">
			<div>Black Wolves: <svg><use xlink:href="#wolf" /></svg></div>
			<div>Gray Wolves: <svg><use xlink:href="#wolf" /></svg></div>
			<div>Unknown Wolves: <svg><use xlink:href="#wolf" /></svg></div>
		</div>
		<div class="packs">
			<?php while ( $crew_members->have_posts() ) : $crew_members->the_post(); ?>
				<article>
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('description'); ?></p>
					<div class="black">
						<span><?php the_field('adult_black_wolves'); ?></span>
						<svg>
							<use xlink:href="#wolf" />
						</svg>
					</div>
					<div class="gray">
						<span><?php the_field('adult_gray_wolves'); ?></span>
						<svg>
							<use xlink:href="#wolf" />
						</svg>
					</div>
					<div class="unknown">
						<span><?php the_field('adult_unknown_wolves'); ?></span>
						<svg>
							<use xlink:href="#wolf" />
						</svg>
					</div>
					<div class="yearlings">
						<span>YEARLINGS:</span>
						<?php 
							$black_yearlings = get_field('yearling_black_wolves'); 
							$gray_yearlings = get_field('yearling_gray_wolves'); 
							$unknown_yearlings = get_field('yearling_unknown_wolves'); 
							$total = $black_yearlings + $gray_yearlings + $unknown_yearlings;
						?>
						<?php if ( $total <= 0 ) : ?>
							<span>None</span>
						<?php else : ?>
							<?php while( $black_yearlings > 0) : ?>
								<svg class="black">
									<use xlink:href="#wolf" />
								</svg>
							<?php $black_yearlings--; endwhile; ?>
							<?php while( $gray_yearlings  > 0) : ?>
								<svg class="gray">
									<use xlink:href="#wolf" />
								</svg>
							<?php $gray_yearlings--; endwhile; ?>
							<?php while( $unknown_yearlings  > 0) : ?>
								<svg class="unknown">
									<use xlink:href="#wolf" />
								</svg>
							<?php $unknown_yearlings--; endwhile; ?>
						<?php endif; ?>

					</div>
					<div class="pups">
						<span>PUPS:</span>
						<?php 
							$black_pups = get_field('pup_black_wolves'); 
							$gray_pups = get_field('pup_gray_wolves'); 
							$unknown_pups = get_field('pup_unknown_wolves'); 
							$total = $black_pups + $gray_pups + $unknown_pups;
						?>
						<?php if ( $total <= 0 ) : ?>
							<span>None</span>
						<?php else : ?>
							<?php while( $black_pups > 0) : ?>
								<svg class="black">
									<use xlink:href="#wolf" />
								</svg>
							<?php $black_pups--; endwhile; ?>
							<?php while( $gray_pups  > 0) : ?>
								<svg class="gray">
									<use xlink:href="#wolf" />
								</svg>
							<?php $gray_pups--; endwhile; ?>
							<?php while( $unknown_pups  > 0) : ?>
								<svg class="unknown">
									<use xlink:href="#wolf" />
								</svg>
							<?php $unknown_pups--; endwhile; ?>
						<?php endif; ?>

					</div>
				</article>
			<?php endwhile; ?>
			<div class="more">
				<h3>Learn More</h3>
				<p>To learn more about the packs visit the Yellowstone Wolf Family Tree.  Jim Halfpenny and Leo Leckie have been documenting the pedigree and heritage of wolf restoration from 1995 to the present. To request an invitation to view the family tree, go to <a href="http://www.wolfgenes.info/" target="_blank">www.wolfgenes.info</a> and select the Ancestry tab.</p>
			</div>
		</div>
	</section>
<?php endif; wp_reset_postdata(); ?>
