<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<section>
		<div>
			<?php dynamic_sidebar( 'footer-one' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'footer-two' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'footer-three' ); ?>
		</div>
		<div>
			<?php dynamic_sidebar( 'footer-four' ); ?>
		</div>
	</section>
	<section class="copyright">
		<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		<nav>
			<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
		</nav>
	</section>
	<div id="element5-credit" data-emergence="hidden">
		<a target="_blank" href="https://element5digital.com">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
</footer>