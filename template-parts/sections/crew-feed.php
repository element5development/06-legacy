<?php 
/*----------------------------------------------------------------*\

	CREW FEED

\*----------------------------------------------------------------*/
?>
	
<?php // Custom WP query crew_members
	$args_crew_members = array(
		'post_type' => array('crew'),
		'posts_per_page' => -1,
	);
	$crew_members = new WP_Query( $args_crew_members );
?>

<?php if ( $crew_members->have_posts() ) : ?>
	<section class="crew-feed has-large-width">
		<?php while ( $crew_members->have_posts() ) : $crew_members->the_post(); ?>
		<article>
			<div>
				<?php 
					$thumbnail_id = get_post_thumbnail_id( $post->ID ); 
					$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(),'small');
					$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
				?>
				<img src="<?php echo $thumbnail_url; ?>" alt="<?php echo $alt; ?>" />
			</div>
			<h3><?php the_title(); ?></h3>
			<p><?php the_field('description'); ?></p>
		</article>
		<?php endwhile; ?>
	</section>
<?php endif; wp_reset_postdata(); ?>
