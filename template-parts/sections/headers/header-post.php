<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>


<?php 
	//BACKGROUND IMAGE?
	if ( has_post_thumbnail() && 'galleries' != get_post_type() && 'legislation' != get_post_type() ) :
		$class = 'has-image';
		$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
	else:
		$class = '';
		$background = '';
	endif;
	$cats = get_the_category(get_the_ID());
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background; ?>');">
	<section>
		<?php if ( $cats ) : ?>
			<p><?php echo $cats[0]->name; ?></p>
		<?php endif; ?>
		<h1><?php the_title(); ?></h1>
	</section>
	<?php if ( has_post_thumbnail() && 'galleries' != get_post_type() && 'legislation' != get_post_type() ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>