<?php 
/*----------------------------------------------------------------*\

	HEADER FOR SEARCH RESULTS

\*----------------------------------------------------------------*/
?>

<header class="page-title">
	<section>
		<h1>Search</h1>
		<?php echo get_search_form(); ?>
	</section>
</header>