<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	$posttype = get_query_var('post_type');
	//SET DEFAULT POST TYPE
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
	//BACKGROUND IMAGE?
	if ( $posttype == 'legislation' ) :
		$class = 'has-image';
		$background = get_field('legislation_background_image','option');
	else:
		$class = '';
		$background = '';
	endif;
?>

<header class="page-title <?php echo $class; ?>" style="<?php if ( $background != '' ) : ?>background-image: url('<?php echo $background['sizes']['xlarge']; ?>');<?php endif; ?>">
	<section>
		<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
	</section>
	<?php if ( $posttype == 'legislation' ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>