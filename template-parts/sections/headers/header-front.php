<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

<?php 
//BACKGROUND IMAGE?
if ( has_post_thumbnail() ) :
	$class = 'has-image';
	$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
else:
	$class = '';
	$background = '';
endif;
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background; ?>');">
	<section>
		<h1><?php the_title(); ?></h1>
		<?php if ( get_field('title_description') ) : ?>
			<p><?php the_field('title_description'); ?></p>
		<?php endif; ?>
		<?php if ( get_field('title_button') ) : ?>
			<?php
				$link = get_field('title_button');
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		<?php endif; ?>
	</section>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>