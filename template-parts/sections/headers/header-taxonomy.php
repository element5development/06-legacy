<?php 
/*----------------------------------------------------------------*\

	HEADER FOR TAXONOMIES

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	$posttype = get_query_var('post_type');
	//SET DEFAULT POST TYPE
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
	//BACKGROUND IMAGE?
	$background = get_field('legislation_background_image','option');
?>

<header class="page-title has-image" style="background-image: url('<?php echo $background['sizes']['xlarge']; ?>');">
	<section>
		<h1><?php single_term_title(); ?></h1>
	</section>
	<div class="overlay"></div>
</header>