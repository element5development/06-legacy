<?php 
/*----------------------------------------------------------------*\

	STANDARD WYSIWYG

\*----------------------------------------------------------------*/
?>

<section class="help-out">
	<h2>Want to help protect wolves?</h2>
	<div>
		<div>
			<div>
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Volunteer.jpg" alt="Support With Time" />
			</div>
			<h3>Volunteer</h3>
			<p>We need advocates like you to help us save the gray wolf. Contact us today to find out how you can get involved in an upcoming mission.</p>
			<div>
				<a href="<?php the_permalink(382); ?>" class="button">Learn More</a>
			</div>
		</div>
		<div>
			<div>
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/Donate.jpg" alt="Financially Support" />
			</div>
			<h3>Donate</h3>
			<p>Your donation supports our mission to protect the gray wolf. Through education, awareness, and legislative change we can make a difference for their species and help to protect wolves for generations.</p>
			<div>
				<a href="<?php the_permalink(371); ?>" class="button">Donate Now</a>
			</div>
		</div>
	</div>
</section>