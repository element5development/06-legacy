<?php
$id = 0;
while ( have_rows('article') ) : the_row();
	$id++;
	if( get_row_layout() == 'basic_editor' ):
		hm_get_template_part( 'template-parts/sections/article/wysiwyg', [ 'sectionId' => $id ] );
	elseif( get_row_layout() == 'two_col_editor' ):
		hm_get_template_part( 'template-parts/sections/article/wysiwyg-two', [ 'sectionId' => $id ] );
	elseif( get_row_layout() == 'media_and_text' ):
		hm_get_template_part( 'template-parts/sections/article/media-text', [ 'sectionId' => $id ] );
	elseif( get_row_layout() == 'banner' ):
		hm_get_template_part( 'template-parts/sections/article/banner', [ 'sectionId' => $id ] );
	elseif( get_row_layout() == 'gallery' ):
		hm_get_template_part( 'template-parts/sections/article/gallery', [ 'sectionId' => $id ] );
	endif;
endwhile;
?>