<?php 
/*----------------------------------------------------------------*\

	VARIOUS THEME FUNCTIONS AND SETUP
	Refer to the Lib folder to view, edit, add funcions

\*----------------------------------------------------------------*/

$file_includes = [
  'lib/theme_support.php',          // General
  'lib/soil_setup.php',             // WordPress Clean Up
  'lib/media_setup.php',            // Images Sizes and File Types
  'lib/gf_setup.php',               // Gravity Forms
	'lib/acf_setup.php',              // Adavance Custom Fields
	'lib/blocks.php',              		// Modify WP default Blocks
  'lib/roles.php',                  // User Role Capabilities
  'lib/whitelabel.php',             // Admin Area Whitelabel
	'lib/menus.php',                  // Menu Initialization
	'lib/widget_areas.php',           // Widget Area Initialization
  'lib/post_types.php',             // Post Type Initialization
  'lib/taxonomies.php',             // Taxonomiy Initialization
	'lib/shortcodes.php',       			// Shortcode Initialization

];

foreach ($file_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'starting-point'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);