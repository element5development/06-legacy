<?php

/*----------------------------------------------------------------*\

	CUSTOM MENU AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_left_nav' => __( 'Primary Left' ),
		'primary_right_nav' => __( 'Primary Right' ),
		'footer_one_nav' => __( 'Footer One' ),
		'footer_two_nav' => __( 'Footer Two' ),
		'legal_nav' => __( 'Legal' )
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );

/*----------------------------------------------------------------*\
	ENABLE YOAST BREADCRUMBS
\*----------------------------------------------------------------*/
add_theme_support( 'yoast-seo-breadcrumbs' );
function jb_crumble_bread($link_text, $id) {
	$link_text = html_entity_decode($link_text);
	$crumb_length = strlen( $link_text );
 	$crumb_size = 14;
 	$crumble = substr( $link_text, 0, $crumb_size );
	if ( $crumb_length > $crumb_size ) {
		$crumble .= '...';
	}
	return $crumble;
}
add_filter('wp_seo_get_bc_title', 'jb_crumble_bread', 10, 2);