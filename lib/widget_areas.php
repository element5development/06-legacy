<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
function widget_areas() {
	$args = array(
		'name'          => __( 'Footer One' ),
		'class'         => 'footer-one',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Two' ),
		'class'         => 'footer-two',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Three' ),
		'class'         => 'footer-three',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
	$args = array(
		'name'          => __( 'Footer Four' ),
		'class'         => 'footer-four',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'widget_areas' );