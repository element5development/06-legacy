<?php

/*----------------------------------------------------------------*\
	EXTERNAL JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*----------------------------------------------------------------*\
	ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------------*\
	ADD & CUSTOMIZE EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
function get_excerpt($limit, $source = null){
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'…';
	return $excerpt;
}

/*----------------------------------------------------------------*\
	MAIN LOOP TO IGNORE STICKY POSTS
\*----------------------------------------------------------------*/
function remove_sticky_from_main_loop( $query ) {
	if( $query->is_main_query() ) {
		$query->set( 'ignore_sticky_posts', true );
	}
}
add_action( 'pre_get_posts', 'remove_sticky_from_main_loop' );

/*----------------------------------------------------------------*\
 Like get_template_part() put lets you pass args to the template file
 Args are available in the tempalte as $template_args array
 @param string filepart
 @param mixed wp_args style argument list
\*----------------------------------------------------------------*/
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
	$template_args = wp_parse_args( $template_args );
	$cache_args = wp_parse_args( $cache_args );
	if ( $cache_args ) {
			foreach ( $template_args as $key => $value ) {
					if ( is_scalar( $value ) || is_array( $value ) ) {
							$cache_args[$key] = $value;
					} else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
							$cache_args[$key] = call_user_method( 'get_id', $value );
					}
			}
			if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
					if ( ! empty( $template_args['return'] ) )
							return $cache;
					echo $cache;
					return;
			}
	}
	$file_handle = $file;
	do_action( 'start_operation', 'hm_template_part::' . $file_handle );
	if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
			$file = get_stylesheet_directory() . '/' . $file . '.php';
	elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
			$file = get_template_directory() . '/' . $file . '.php';
	ob_start();
	$return = require( $file );
	$data = ob_get_clean();
	do_action( 'end_operation', 'hm_template_part::' . $file_handle );
	if ( $cache_args ) {
			wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
	}
	if ( ! empty( $template_args['return'] ) )
			if ( $return === false )
					return false;
			else
					return $data;
	echo $data;
}