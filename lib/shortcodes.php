<?php

/*----------------------------------------------------------------*\
	BUTTON SHOTCODE
\*----------------------------------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
		'url' => '#',
		'scroll' => '',
		'type' => '',
		'color' => '',
		'size' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.' is-'.$color.' is-'.$size.' '.$scroll.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('button', 'cta_button');
/*----------------------------------------------------------------*\
	CREW FEED SHOTCODE
\*----------------------------------------------------------------*/
function get_crew( $attr ) {
	ob_start();
	get_template_part( 'template-parts/sections/crew-feed' );
	return ob_get_clean();
}
add_shortcode( 'crew', 'get_crew' );
/*----------------------------------------------------------------*\
	PACK FEED SHOTCODE
\*----------------------------------------------------------------*/
function get_pack( $attr ) {
	ob_start();
	get_template_part( 'template-parts/sections/pack-feed' );
	return ob_get_clean();
}
add_shortcode( 'pack', 'get_pack' );