<?php
/*----------------------------------------------------------------*\
	AUTO CLOSE ACF
\*----------------------------------------------------------------*/
function my_acf_admin_head() {
	?>
	<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$('.layout').addClass('-collapsed');
				$('.acf-postbox').addClass('closed');
			});
		})(jQuery);
	</script>
	<?php
 }
 add_action('acf/input/admin_head', 'my_acf_admin_head');

/*----------------------------------------------------------------*\
	OPTION PAGES
\*----------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'menu_slug' 	=> 'archives-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
?>