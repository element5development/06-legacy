<?php

/*----------------------------------------------------------------*\
	SPECIFY WHAT BLOCKS ARE ALLOWED
\*----------------------------------------------------------------*/
function e5_allowed_block_types( $allowed_blocks ) {
	return array(
		'core/heading',
		'core/paragraph',
		'core/quote',
		'core/list',
		'core/image',
		'core/gallery',
		'core/cover-image',
		'core/file',
		'core/table',
		'core/freeform',
		'core/html',
		'core/button',
		'core/text-columns',
		'core/media-text',
		'core/separator',
		'core/spacer',
		'core/shortcode',
		'core/embed',
		'core-embed/youtube',
		'core-embed/vimeo',
		'core-embed/wordpress',
	);
}
add_filter( 'allowed_block_types', 'e5_allowed_block_types' );
/*----------------------------------------------------------------*\
	DISABLE BLOCK CUSTOM COLORS AND FONT SIZES
\*----------------------------------------------------------------*/
function gutenberg_disable_custom_options() {
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'disable-custom-font-size' );
}
add_action( 'after_setup_theme', 'gutenberg_disable_custom_options' );
/*----------------------------------------------------------------*\
	SET BLOCK FONT SIZES
\*----------------------------------------------------------------*/
function gutenberg_font_sizes() {
	add_theme_support( 
		'editor-font-sizes', array(
			array(
				'name'      => __( 'small' ),
				'shortName' => __( 'S'),
				'size'      => 14,
				'slug'      => 'small'
			),
			array(
				'name'      => __( 'regular' ),
				'shortName' => __( 'R'),
				'size'      => 18,
				'slug'      => 'regular'
			),
			array(
				'name'      => __( 'medium' ),
				'shortName' => __( 'M'),
				'size'      => 26,
				'slug'      => 'medium'
			),
			array(
				'name'      => __( 'large' ),
				'shortName' => __( 'L'),
				'size'      => 34,
				'slug'      => 'large'
			),
		)
	);
}
add_action( 'after_setup_theme', 'gutenberg_font_sizes' );
/*----------------------------------------------------------------*\
	SET BLOCK COLOR PALETTE
\*----------------------------------------------------------------*/
function gutenberg_color_palette() {
	add_theme_support(
		'editor-color-palette', array(
			array(
				'name'  => esc_html__( 'Black' ),
				'slug' => 'black',
				'color' => '#1B171F',
			),
			array(
				'name'  => esc_html__( 'Dark Grey' ),
				'slug' => 'dark-grey',
				'color' => '#454148',
			),
			array(
				'name'  => esc_html__( 'Grey' ),
				'slug' => 'grey',
				'color' => '#C9C7B9',
			),
			array(
				'name'  => esc_html__( 'Light Grey' ),
				'slug' => 'light-grey',
				'color' => '#E8E8EF',
			),
			array(
				'name'  => esc_html__( 'White' ),
				'slug' => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => esc_html__( 'Brown' ),
				'slug' => 'brown',
				'color' => '#905D30',
			),
			array(
				'name'  => esc_html__( 'Light Brown' ),
				'slug' => 'light-brown',
				'color' => '#CAA07C',
			),
		)
	);
}
add_action( 'after_setup_theme', 'gutenberg_color_palette' );

/*----------------------------------------------------------------*\
	DISABLE WP BLOCKS BASED ON TEMPLATE & FRONT PAGE
\*----------------------------------------------------------------*/
function disable_editor_by_template( $id = false ) {
	$excluded_templates = array(
		'templates/acf-builder.php',
		'templates/forms.php',
		'templates/forms-2-column.php',
		'templates/confirmation.php',
	);
	$excluded_ids = array(
		get_option( 'page_on_front' )
	);
	if( empty( $id ) )
		return false;
	$id = intval( $id );
	$template = get_page_template_slug( $id );
	return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
}
function disable_gutenberg_by_template( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;
	if( disable_editor_by_template( $_GET['post'] ) )
		$can_edit = false;
	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'disable_gutenberg_by_template', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'disable_gutenberg_by_template', 10, 2 );
/*----------------------------------------------------------------*\
	DISABLE WP BLOCKS BASED ON POST TYPE
\*----------------------------------------------------------------*/
function prefix_disable_gutenberg($current_status, $post_type) {
    // Use your post type key instead of 'product'
		if ($post_type === 'crew' || $post_type === 'pack' || $post_type === 'legislation' ) 
			return false;
    return $current_status;
}
add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);