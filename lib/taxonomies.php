<?php

/*----------------------------------------------------------------*\

	CUSTOM TAXONOMIES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Taxonomy State
function create_state_tax() {

	$labels = array(
		'name'              => _x( 'States', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'State', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search States', 'textdomain' ),
		'all_items'         => __( 'All States', 'textdomain' ),
		'parent_item'       => __( 'Parent State', 'textdomain' ),
		'parent_item_colon' => __( 'Parent State:', 'textdomain' ),
		'edit_item'         => __( 'Edit State', 'textdomain' ),
		'update_item'       => __( 'Update State', 'textdomain' ),
		'add_new_item'      => __( 'Add New State', 'textdomain' ),
		'new_item_name'     => __( 'New State Name', 'textdomain' ),
		'menu_name'         => __( 'State', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'state', array('legislation'), $args );

}
add_action( 'init', 'create_state_tax' );