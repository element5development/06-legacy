<?php 
/*----------------------------------------------------------------*\

	DEFAULT ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header-archives'); ?>

<main>
	<article> 
		<!-- CURRENT LEGISLATION -->
		<?php
			$args = array(
				'posts_per_page' => -1,
				'post_type' => array('legislation'),
				'meta_query' => array(
					array(
							'key' => 'legislation_status', // name of custom field
							'value' => 'Current',
					)
				)
			);
			$current_query = new WP_Query($args);
		?>
		<?php if ( $current_query->have_posts() ) : ?>
			<section class="upcoming-post has-large-width">
			<div class="state-select">
				<label>Search legislation by state</label>
				<?php
					$categories = get_categories('taxonomy=state');

					$select = "<select name='cat' id='cat' class='postform'>n";
					$select.= "<option value='-1'>Select State</option>n";
				
					foreach($categories as $category){
						if($category->count > 0){
								$select.= "<option value='".$category->slug."'>".$category->name."</option>";
						}
					}
				
					$select.= "</select>";
				
					echo $select;
				?>
				<script type="text/javascript"><!--
					var dropdown = document.getElementById("cat");

					function onCatChange() {
						if (dropdown.options[dropdown.selectedIndex].value != -1) {
							location.href = "<?php echo get_site_url(); ?>/state/" + dropdown.options[dropdown.selectedIndex].value + "/";
						}
					}
					dropdown.onchange = onCatChange;
				--></script>
				</div>

				<h2>Current Legislation</h2>
				<div class="current-wrap">
					<?php while ( $current_query->have_posts() ) : $current_query->the_post(); ?>
						<?php get_template_part('template-parts/elements/previews/preview-' . $posttype); ?>
					<?php endwhile; ?>
				</div>
			</section>
			<section class="infinite-scroll has-normal-width">
				<div class="page-load-status">
					<p class="infinite-scroll-request">
						<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
							<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
								<animateTransform attributeType="xml"
									attributeName="transform"
									type="rotate"
									from="0 25 25"
									to="360 25 25"
									dur="0.6s"
									repeatCount="indefinite"/>
							</path>
						</svg>
					</p>
					<p class="infinite-scroll-last"></p>
					<p class="infinite-scroll-error"></p>
				</div>
				<?php the_posts_pagination( array(
					'prev_text'	=> __( 'Previous page' ),
					'next_text'	=> __( 'Next page' ),
				) ); ?>
				<a class="load-more button is-black">Load More</a>
			</section>
			<section class="upcoming-post has-large-width">
				<div class="state-sidebar">
					<h3>Legislation by State</h3>
					<?php $terms = get_terms( 'state' ); ?>
					<?php if ( ! empty( $terms ) ) : ?>
						<ul>
							<?php foreach ( $terms as $term ) : ?>
								<li><a href="<?php echo get_site_url(); ?>/state/<?php echo $term->slug; ?>/"><?php echo esc_html( $term->name ); ?></a></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</section>
		<?php; endif; wp_reset_postdata();	?>
	</article>
</main>

<?php get_template_part('template-parts/sections/wildlife-champs'); ?>

<?php get_template_part('template-parts/sections/help-out'); ?>

<?php get_template_part('template-parts/sections/footers/footer-newsletter'); ?>

<?php get_footer(); ?>