<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title">
	<section>
		<h1>Lost Page</h1>
	</section>
</header>

<main>
	<article class="paper-container">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/lost-in-valley.svg" alt="valley landscape icon" />
		<section>
			<h2>It seems this page took a yellowstone tour and veered off path.</h2>
			<p>Don't worry we have sent out a search party. In the mean time you can follow the <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">news</a> or head <a href="<?php echo get_home_url(); ?>">home</a> and rest.</p>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>