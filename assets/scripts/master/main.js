var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.feed .preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		history: false,
		status: '.page-load-status',
	});
	/*----------------------------------------------------------------*\
  	RESIZE LOGO ON SCROLL
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 300) {
			$("nav.primary-nav").addClass("is-minimal");
		} else {
			$("nav.primary-nav").removeClass("is-minimal");
		}
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$("#activate-search").click(function () {
		$(this).toggleClass("is-active");
		$(".search-form").toggleClass("is-active");
		$(".search-form form input").focus();
	});
	$("#close-search").click(function () {
		$(".search-form").toggleClass("is-active");
		$(".search-form form input").focus();
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("#activate-menu").click(function () {
		$(".menu-items-container").toggleClass("is-active");
	});
	$("#close-menu").click(function () {
		$(".menu-items-container").toggleClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	ACTIVE SHARE ELEMENT
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 350) {
			$(".social-share").addClass("is-active");
		} else {
			$(".social-share").removeClass("is-active");
		}
	});
	/*----------------------------------------------------------------*\
  	PARALLAX ELEMENTS
	\*----------------------------------------------------------------*/
	if ($('.rellax').length) {
		var rellax = new Rellax('.rellax');
	}
	/*----------------------------------------------------------------*\
  	IMAGE MAP
	\*----------------------------------------------------------------*/
	if ($('#map-image').length) {
		$('map').imageMapResize();
	}
	/*----------------------------------------------------------------*\
  	BACK TO TOP
	\*----------------------------------------------------------------*/
	var amountScrolled = 1024;

	$(window).scroll(function () {
		if ($(window).scrollTop() > amountScrolled) {
			$('#back-to-top').addClass('show');
		} else {
			$('#back-to-top').removeClass('show');
		}
	});

	$('#back-to-top').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
});