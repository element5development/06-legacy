var $ = jQuery;

$(document).ready(function () {
	$('a[href^="#"]').bind('click', function (event) {
		event.preventDefault(); // prevent hard jump, the default behavior

		var target = $(this).attr("href"); // Set the target as variable

		// perform animated scrolling by getting top-position of target-element and set it as scroll target
		$('html, body').stop().animate({
			scrollTop: $(target).offset().top - 150
		}, 600, function () {
			// update focus for accessibility
			// var $target = $(target);
			// $target.focus();
			// if ($target.is(":focus")) {
			// 	return false;
			// } else {
			// 	$target.attr('tabindex', '-1');
			// 	$target.focus();
			// };
		});
	});
});