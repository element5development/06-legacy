<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	page template which takes advantage of the WordPress block system,
	however some default blocks and settings have been removed.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php if ( !is_page(808) ) : 
	get_template_part('template-parts/sections/help-out'); 
endif; ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>