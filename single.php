<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header-post'); ?>

<main>
	<article>
		<?php get_template_part('template-parts/elements/share'); ?>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/related'); ?>

<?php if ( comments_open() || get_comments_number() ) : ?>
	<?php comments_template(); ?>
<?php endif; ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>