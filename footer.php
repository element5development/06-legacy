<?php 
/*----------------------------------------------------------------*\

	HTML FOOTER CONTENT
	Commonly only used to close off open containers
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<button id="back-to-top">
	<svg viewBox="0 0 32 32">
		<path d="M.586 21L6 26.414l10-10 10 10L31.414 21 16 5.586z" fill="#caa07c"/>
	</svg>
	Back to Top
</button>

<?php wp_footer(); ?>

</body>

</html>