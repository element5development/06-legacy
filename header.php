<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-55M9ZZV');</script>
	<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="msvalidate.01" content="D7FF62B50B5AC4465FB383958CD4A132" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>
		<?php wp_title(''); ?>
	</title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55M9ZZV"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php get_template_part('template-parts/icon-set'); ?>