<?php 
/*----------------------------------------------------------------*\

	SINGLE GALLERY TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header-post'); ?>

<main>
	<article>
		<?php get_template_part('template-parts/elements/share'); ?>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>